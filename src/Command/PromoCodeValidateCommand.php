<?php

namespace App\Command;

use App\Service\PromoCodeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    protected static $defaultDescription = 'Validate a promo code';
    private $promoCodeService;

    protected function configure(): void
    {
        $this
            ->addArgument('code', InputArgument::REQUIRED, 'Argument description')
        ;
    }

    public function __construct(PromoCodeService $promoCodeService) {
        parent::__construct();
        $this->promoCodeService = $promoCodeService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $code = $input->getArgument('code');
        @unlink($this->promoCodeService->getJsonFileName($code));
        $promoData = $this->promoCodeService->getPromoCodeData($code);
        if($promoData) {
            $this->promoCodeService->generateJson($promoData);
            $io->success('Promo code validated');
        } else {
            echo 'no';
            $io->error('Promo code not validated');
        }


        return Command::SUCCESS;
    }
}
