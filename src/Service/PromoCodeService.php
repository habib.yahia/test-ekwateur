<?php

namespace App\Service;

class PromoCodeService {
    public function getPromoCodeData($code): ?array {
        $promoCode = $this->getPromoCode($code);
        if($promoCode) {
            $offers = $this->getCompatibleOffers($code);
            $promoCode['compatibleOfferList'] = $offers;
            return $promoCode;
        }
        return null;
    }

    public function getAllPromoCodes(): array {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch), true);
    }

    public function getOffersList(): array {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://601025826c21e10017050013.mockapi.io/ekwatest/offerList");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch), true);
    }

    public function getPromoCode($code): ?array {
        $promoCodes = $this->getAllPromoCodes();
        $promoCodeKey = array_search($code, array_column($promoCodes, 'code'));
        if($promoCodeKey !== false)
            return $promoCodes[$promoCodeKey];
        else {
            return null;
        }
    }

    public function getCompatibleOffers($code): ?array {
        $offersFound = array();
        $offers = $this->getOffersList();
        foreach($offers as $offer) {
            if(in_array($code, $offer['validPromoCodeList'])) {
                $offersFound[] =  array(
                    'name' => $offer['offerName'],
                    'type' => $offer['offerType']
                );
            }
        }
        if(count($offersFound) > 0)
            return $offersFound;
        return null;
    }

    public function generateJson($promoCode): void {
        file_put_contents($this->getJsonFileName($promoCode['code']), json_encode($this->cleanPromoCodeKeys($promoCode)));
    }

    public function cleanPromoCodeKeys($promoCode): array {
        $promoCode['promoCode'] = $promoCode['code'];
        unset($promoCode['code']);
        return $promoCode;
    }

    public function getJsonFileName($code): string {
        return "promo_code_$code.json";
    }

}
