<?php

namespace App\Tests\Command;

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PromoCodeValidateTest extends KernelTestCase
{
    public function testPromoCodeValidated(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code' => 'ALL_2000',
        ]);

        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Promo code validated', $output);
        $this->assertFileExists('promo_code_ALL_2000.json');
    }

    public function testPromoCodeNotValidated(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code' => 'WRONG-PROMOCODE',
        ]);

        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Promo code not validated', $output);
        $this->assertFileNotExists('promo_code_WRONG-PROMOCODE.json');
    }
}
